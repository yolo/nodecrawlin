/**
 * Created by kingHenry on 10/4/14.
 */
var express = require('express');
var fs = require('fs-extra');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();
var async = require('async');
var _ = require('underscore');
var colors=require('colors') ;
var kue = require('kue'),
    jobs=kue.createQueue();


var  goodSeed=['http://www.alise.org/alise-membership---2014---institutional-members'];
var  badSeed1=['http://www.library.ualberta.ca/'] ;
var  badSeed2=['http://www.sis.uottawa.ca/'] ;

var  seedUrl=goodSeed

var wStream=fs.createWriteStream('output.html');
//----------------------------------------------------------

var dontCrawl= [
".png", ".gif", ".css", ".js", ".bmp", ".tiff", ".mid", ".mp2", ".mp3", ".mp4", ".wav", ".avi", ".mov", ".mpeg",
".ram", ".m4v", ".pdf", ".rm", ".smil", ".wmv", ".swf", ".wma", ".tgz", ".gz", ".rar", ".zip", ".jpeg", "rss2", ".xml"
]

var leafUrlCount=0

var crawlUrls= function (level,urlsToCrawl,breadcrumb) {
    var myJobs = [];
    var bCrumb="seedUrl";
    urlsToCrawl.forEach(function(oneUrl,index)
    {
        if(breadcrumb != "seedURL"){
            bCrumb=breadcrumb+'|'+oneUrl;
        }

        skipUrl=false
        for (i=0; i<dontCrawl.length; i++ ) {
           if ( oneUrl.indexOf( dontCrawl[i] ) != -1 ) {
               skipUrl=true
               break
           }
        }

        if  ( ! skipUrl) {
            var crawlJob = jobs.create('S2crawler', {
                url: oneUrl
               ,level:level+1
               ,breadcrumb:bCrumb
            }).save();
            myJobs.push(crawlJob)
        } else  {
             console.log ("Skipping " + oneUrl)
        }
    });

    console.log( "Level="+ level +  " Firing " + myJobs.length + " jobs for" + bCrumb);
    var jobCount=0;

    myJobs.forEach(function(job,index)
    {
        job.on('error', function (error){
            console.log('some shitty ' + error);
       });

      job.on('complete',function(result){
          jobCount+=1;
          try  {
              console.log( jobCount+ " Done Got: " + result.foundUrls.length + " Urls for "  +  job.data.breadcrumb)
              if (result.level<2) {
                  console.log('cb initiating level: '+(result.level+1))
                  if ( result.foundUrls.length > 0 )  {
                     crawlUrls(result.level,result.foundUrls,result.breadcrumb)
                  }
              } else {
                  lvl3UrlArray=result.foundUrls;
                  leafUrlCount+=lvl3UrlArray.length
                  lvl3Breadcrumb="Job " + jobCount +" Overall Leaf Url Count: " + leafUrlCount + '<br><b>'+result.breadcrumb+'</b></br>\n<ol>\n';
                  wStream.write(lvl3Breadcrumb,function(err){
                   if(err){
                     console.log(err);
                    }
                  });
                  for (i=0;i<lvl3UrlArray.length;i++){
                     var theUrl= "<li><a href="+lvl3UrlArray[i]+">"+lvl3UrlArray[i]+"</a></br>\n</li>\n"
                      wStream.write(theUrl,function(err){
                        if(err){
                         console.log(err);
                       }
                        
                      });
                  }

                      wStream.write('</ol>',function(err){
                        
                        if(err){
                         console.log(err);
                       }
                        
                      });
                  }
                  console.log ("LVL2 -->", result.foundUrls)
          } catch(err)  {
               console.log ( "ERROR".red +  result  )
          }
      })

      job.on('error', function(error){
          console.log (jobCount + " Failure for " + job.data.breadcrumb )
      })
    });
};

//------------------------------------------
// MAIN 

crawlUrls(0,seedUrl,"seedURL");
console.log ("ALL DONE ...................")
// kue.app.listen(3000)
