/**
 * Created by kingHenry on 10/4/14.
 */
var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();
var async = require('async');
var _ = require('underscore');
var kue = require('kue'),
    jobs=kue.createQueue();
var colors= require('colors') ;
//require('v8-profiler');


console.log('Start Consumer')
numPage = 1;
var blackList=[];
var MAXLEVELS=3

/*   'http://www.library.ualberta.ca/' ,
   'http://www.dustars.com/',
   'http://www.arts.uottawa.ca/eng/programs/aboriginalstudies.html',
   'http://www.research.uottawa.ca/resources-funding-infrastructure.html',
   'http://www.uottawa.ca/academic/info/regist/calendars/index.html?expandNavSection=stud-section',
   'http://instagram.com/catholicuniversity',
                    ://github.com/hariDasu/nodeCrawlin.gitrlInnerResp=response2.request.uri.href    // response2 for the URL we had asked a while back
   'http://www.clariongoldeneagles.com/',
   'http://www.youtube.com/user/ClarionUniversity1'*/


//---------------------------------------------
// nothing asynchronous in this routine

linksOnThisPage=function(rawResponse, rawHtml) {
    var respHref=rawResponse.request.uri.href
    var absLinks=[]
    var $ = cheerio.load(rawHtml);
    $('body').filter(function () {
        var data = $(this);
        var links = [];
        data.find('a').each(function () {
            var actualLink = String(this.attribs.href)
            if (typeof actualLink === 'string') {
                links.push(actualLink);
            }
        });
        links.splice(0, 1);
        links.forEach(function (actualLink, index, allItems) {
            if ((actualLink.indexOf("http:")) != -1) {
                absLinks.push(actualLink);
            }
        });
    } )
    var uniqueLinks = _.uniq(absLinks);   //  links to crawl next level   
    return (uniqueLinks)
}


//---------------------------------------------
// nothing asynchronous in this routine

isACollege=function(rawResponse, rawHtml) {
    var respHref=rawResponse.request.uri.href
    var $ = cheerio.load(rawHtml);
    retVal=false

    $('body').filter(function () {
        var univs=$(this);
        var titleText = univs.text();
        if( titleText.indexOf("niversity")!=-1||titleText.indexOf("ollege")!=-1||titleText.indexOf("chool")!=-1){
            console.log(respHref, "University/College found ..".bold.blue)
            // done(null,results);
            retVal=true
        } else {
            console.log(respHref, "NOT a College".red)
        };
    });
    return retVal
}


//--------------------------------------------------

jobs.process('S2crawler',function(job,done) {
    url2Crawl = job.data.url;
    breadcrumb=job.data.breadcrumb;
    level=job.data.level;
    console.log( "Lvl=" + level + " Bread Crumb=".green + breadcrumb + "[NEW JOB]" )
    //---  Blacklist check

    if ( _.contains(blackList, url2Crawl) ) {
        console.log("SKIP blacklisted ..".cyan )
        done("blackListed")

    } else  {
        request( {url: url2Crawl, timeout: 7000 }, function(error, response, html){
            if (error) {
                console.log ("req ERR for " + error)
                done(error)

            } else {
                switch (level) {
                    case (1) :                  //  seed page 
                        var linksOnPage=linksOnThisPage(response, html)
                        var returnResults = {
                            foundUrls:linksOnPage,
                            breadcrumb:breadcrumb,
                            level:level
                        };
                        console.log ("Lvl:" + level + " Done ... ".yellow + breadcrumb )
                        done(null,returnResults)
                        break ;

                    case (2) :       // is it a college ?
                        if ( isACollege(response, html) ) {
                            var linksOnPage=linksOnThisPage(response, html)
                            var returnResults = {
                                foundUrls:linksOnPage,
                                breadcrumb:breadcrumb,
                                level:level
                            };
                            console.log ("Lvl:" + level + " Done ... ".yellow + breadcrumb )
                            done(null,returnResults)
                        } else {
                            done(error)
                        }
                        break;
                }
            }
        }).pipe( fs.createWriteStream('p'+numPage+'.html') )  
        numPage+=1;
    }
});   // jobs

